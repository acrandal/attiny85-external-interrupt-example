/*
 *  ATTiny85 simple interrupt handling example
 *  
 *  Pieced together from various places, but mostly from here:
 *  https://arduino.stackexchange.com/questions/3929/attiny85-interrupt-id-vs-pin-when-programming-with-arduino
 *  
 *  Contributors:
 *    GisMofx <https://arduino.stackexchange.com/users/11075/gismofx>
 *    Aaron S. Crandall <acrandal@gmail.com> - 2019
 *    
 *  This code reacts to a Pin Change Interrupt on PB1 which switches an LED on and off on PB4.
 *  This is an "external interrupt" in some documents and other platforms
 *  
 *  To wire this example;
 *    Connect a switch that connects PB1 (pin 6) of the ATtiny85 to ground when pressed
 *    Connect an LED with:
 *      positive terminal (anode - the long lead) to PB4 (pin 3)
 *      negative terminal (cathode - the short lead) to a resistor (300 to 2k ohms all work)
 *    Connect resistor between LED and ground bus
 *    Connect ATtiny85 pin 8 (vcc) to +5 volts
 *    Connect ATtiny85 pin 4 (ground) to ground
 *    
 *    Shared under the Creative Commons Attribution 4.0 International license
 *    https://creativecommons.org/licenses/by/4.0/
 *    
 */

// Requires headers for AVR defines and ISR function
#include <avr/io.h>
#include <avr/interrupt.h>


// Define pins for switch the LED, plus the chosen interrupt for reacting to
#define INTERRUPT_PIN PCINT1  // This is PB1 per the schematic
#define INT_PIN PB1           // Interrupt pin of choice: PB1 (same as PCINT1) - Pin 6
#define LED_PIN PB4           // PB4 - Pin 3

/*
 * Alias for the ISR: "PCINT_VECTOR" (Note: There is only one PCINT ISR. 
 * PCINT0 in the name for the ISR was confusing to me at first, 
 * hence the Alias, but it's how the datasheet refers to it)
 */
#define PCINT_VECTOR PCINT0_vect      // This step is not necessary - it's a naming thing for clarity

// FYI: Variables used within ISR must be declared Volatile.
// static volatile byte LEDState; 

// The setup function runs only once when the ALU boots
void setup() {
    pinMode(LED_PIN, OUTPUT);         // Set our chosen LED visual feedback as an output pin (PB4 / Pin 3)
    digitalWrite(LED_PIN, HIGH);      // Blink to show it's booting up and your LED is wired correctly
    delay(500);
    digitalWrite(LED_PIN, LOW);
    delay(500);

    // Code here is the key piece of configuring and enabling the interrupt 
    cli();                            // Disable interrupts during setup
 
    PCMSK |= (1 << INTERRUPT_PIN);    // Enable interrupt handler (ISR) for our chosen interrupt pin (PCINT1/PB1/pin 6)

    GIMSK |= (1 << PCIE);             // Enable PCINT interrupt in the general interrupt mask

    pinMode(INT_PIN, INPUT_PULLUP);   // Set our interrupt pin as input with a pullup to keep it stable

    sei();                            //last line of setup - enable interrupts after setup

}


void loop() {
  // Put any general processing code in the main loop
  // Be aware - if the INT_PIN changes state, the loop will be suspended while the ISR runs to completion
}


// This is the interrupt handler called when there is any change on the INT_PIN
// ISR is defined in the headers - the ATtiny85 only has one handler
ISR(PCINT_VECTOR)
{
  if( digitalRead(INT_PIN) == HIGH ) {
    digitalWrite(LED_PIN, HIGH);
  }else{
    digitalWrite(LED_PIN, LOW);
  }
}
